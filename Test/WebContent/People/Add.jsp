<%@page import="java.util.ArrayList"%>
<%@page import="org.apache.commons.beanutils.BeanUtils"%>
<%@page import="java.util.Map"%>
<%@page import="bean.People"%>
<%@page import="Servlet.OnlineServlet"%>
<%@page import="Servlet.InvalidateServlet"%>
<%@page import="Listener.OnlineListener"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<%
		// 设置编码方式
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		
		People people = new People();
		Map<String, String[]> peopleMap = request.getParameterMap();	
		BeanUtils.populate(people, peopleMap);
		
		ArrayList<People> peoples = (ArrayList<People>)session.getAttribute("peoples");
		peoples.add(people);
		response.sendRedirect("Succ.jsp");
	%>
</body>
</html>