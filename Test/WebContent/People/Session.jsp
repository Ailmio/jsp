<%@page import="Servlet.OnlineServlet"%>
<%@page import="Servlet.InvalidateServlet"%>
<%@page import="Listener.OnlineListener"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Session</title>
</head>
<body>
	<%
		request.setCharacterEncoding("utf-8");
	 	String username = request.getParameter("username");
	 	String password = request.getParameter("password");
	 	session.setAttribute("username", username);
	 	session.setAttribute("password", password);
	 	response.sendRedirect("Succ.jsp"); 		
	 %>
</body>
</html>